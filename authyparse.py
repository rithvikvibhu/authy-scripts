'''
File name: authyparse.py
Author: Rithvik Vibhu
Python version: 3.x

This script processes a given json of Authy totp secrets. Check the README for more info.
'''
import json, csv
from pprint import pprint

JSON_FILENAME = 'authyjson.json'
OUT_CSV_FILENAME = 'authycsv.csv'

# Load data from json file
with open(JSON_FILENAME, 'r') as f:
    data = json.load(f)

def filter_fields(data):
    '''Picks only important fields and returns a dict'''
    final_accounts = {}
    for account in data:
        final_accounts[account['name'].split(':')[0]] = { 'createdDate': account['createdDate'], 'username': str(account['originalName']).split(':')[-1], 'digits': account['digits'], 'secret': account['decryptedSeed'] }
    return final_accounts

def export_to_csv(data):
    '''Exports everything to a csv file'''
    with open(OUT_CSV_FILENAME, 'w') as f:
        fieldnames = data[0].keys()
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()

        for item in data:
            writer.writerow(item)


# Uncomment any line below

# pprint(filter_fields(data))
# export_to_csv(data)