/*
    This snippet extracts totp secrets from Authy Chrome app.
    Inspect main.html from chrome://extensions. Paste code in console.
    The json content is copied to clipboard.
*/

console.clear();
console.warn('Heres Your Authy TOTP Secrets');
console.warn('Click Any Group To Expand / Collapse It');
console.warn('If Nothing Shows Make Sure To Login To Authy & Enter Your Password');
var tokens = [];
appManager.getModel().forEach(function(i){
    if(i.markedForDeletion === false){
        console.groupCollapsed(i.name);
        tokens.push(i);
        console.log('TOTP Secret:',i.markedForDeletion === false ? i.decryptedSeed : hex_to_b32(i.secretSeed));
        console.log('TOTP URI:','otpauth://totp/'+encodeURIComponent(i.name)+'?secret='+i.decryptedSeed+'&issuer='+i.accountType);
        console.log('Google QR Code:','https://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/'+encodeURIComponent(i.name)+'?secret='+i.decryptedSeed+'&issuer='+i.accountType);
        console.groupEnd();
    }
});
console.log(tokens);
copy(tokens);
