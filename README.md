# authy-scripts

This project contains 2 files.

- `authy-extract-snippet.js`
    - A JavaScript snippet to extract TOTP secrets from Authy Chrome App (not the new desktop app)
    - Open the app, and open Google Chrome.
    - Go to chrome://extensions and inspect **main.html**.
    - Switch to the console tab. Paste the snippet in the console.
    - The JSON content will be copied to clipboard.
    - Open a text editor, paste from clipboard, and save the file with **.json** extension.
    - You can now use this file with **authyparse.py**.

- `authyparse.py`
    - This python script parses a json file generated with **authy-extract-snippet.js**
    - Match JSON_FILENAME in the script to the json file you saved earlier.
    - This file has 2 functions:
        - `filter_fields (data)` - This function filters only important fields and prints it.
        - `export_to_csv (data)` - This function exports all data from the json file as csv (saved in same directory).
    - To use these, uncomment any of the last 2 lines and run the script with `python authyparse.py`.
